package ru.ivanrigovsky.eclipse.compositelauncher.constants;

public class ConfigConstant {
	public final static String CONFIGURATIONS_LIST_SELECTED = "compositeLauncer_configurationsListSelected";
	public final static String CONFIGURATIONS_LIST = "compositeLauncer_configurationsList";
	
}
