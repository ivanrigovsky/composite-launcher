package ru.ivanrigovsky.eclipse.compositelauncher.launcher;

import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.CommonTab;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;


@SuppressWarnings("restriction")
public class CompositeLaunchTabGroup extends AbstractLaunchConfigurationTabGroup {
	@Override
	public void createTabs(ILaunchConfigurationDialog dialog, String mode) { 
		ILaunchConfigurationTab[] tabs = new ILaunchConfigurationTab[] { 
			new MainTab(),
			new CommonTab()
		};
		setTabs(tabs);
	} 


}
