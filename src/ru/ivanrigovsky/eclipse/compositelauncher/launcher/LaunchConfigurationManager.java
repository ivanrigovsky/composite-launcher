package ru.ivanrigovsky.eclipse.compositelauncher.launcher;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchManager;

public class LaunchConfigurationManager {
	private ILaunchManager manager;
	private Map<String, ILaunchConfigurationType> types; //<name, type>	
	private Map<ILaunchConfigurationType, ILaunchConfiguration[]> typeConfigurations;
	private Map<String, ILaunchConfiguration> configurations;
	
	private LaunchConfigurationManager() {
		manager = DebugPlugin.getDefault().getLaunchManager();
		
		types = new HashMap<String, ILaunchConfigurationType>();
		for (ILaunchConfigurationType type : manager.getLaunchConfigurationTypes()) {
			types.put(type.getName(), type);
		}
		
		typeConfigurations = new HashMap<ILaunchConfigurationType, ILaunchConfiguration[]>();
		configurations = new HashMap<String, ILaunchConfiguration>();
		for (ILaunchConfigurationType type : types.values()) {
			try {
				ILaunchConfiguration[] typeConfigurationsArray = manager.getLaunchConfigurations(type);
				typeConfigurations.put(type, typeConfigurationsArray);
				for (ILaunchConfiguration configuration : typeConfigurationsArray)
					configurations.put(configuration.getName(), configuration);
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static LaunchConfigurationManager getInstance() {
		return new LaunchConfigurationManager();
	}
	
	public Map<String, ILaunchConfigurationType> getTypes() {
		return  types;
	}
		
	public ILaunchConfiguration[] getConfigurations(ILaunchConfigurationType type) {
		return typeConfigurations.get(type);
	}
	
	public ILaunchConfiguration getConfiguration(String name) throws CoreException {
		return configurations.get(name);
	}
}
