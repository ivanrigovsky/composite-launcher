package ru.ivanrigovsky.eclipse.compositelauncher.launcher;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;

import ru.ivanrigovsky.eclipse.compositelauncher.utils.ConfigDataUtils;


public class CompositeLaunchConfiguration implements ILaunchConfigurationDelegate {

	@Override
	public void launch(ILaunchConfiguration config, String mode, ILaunch iLaunch,
			IProgressMonitor progressMonitor) throws CoreException {
		LaunchConfigurationManager manager = LaunchConfigurationManager.getInstance();
		List<ILaunchConfiguration> orderedSelectedConfigurations = new LinkedList<ILaunchConfiguration>(); 
		ConfigDataUtils.loadData(config, manager, null, null, orderedSelectedConfigurations);
		
		for (ILaunchConfiguration configuration : orderedSelectedConfigurations) {				
			configuration.launch(mode, progressMonitor);
		}
	}
}
