package ru.ivanrigovsky.eclipse.compositelauncher.launcher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.internal.ui.IInternalDebugUIConstants;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import ru.ivanrigovsky.eclipse.compositelauncher.utils.ConfigDataUtils;

@SuppressWarnings("restriction")
public class MainTab extends AbstractLaunchConfigurationTab {
	private final static String TAB_NAME = "Main";
	
	private Combo configurationTypesCombo;
	private Text route;
	private Set<String> typeNames;
	private LaunchConfigurationManager manager;
	
	private List<ILaunchConfiguration> configurationsList = new LinkedList<ILaunchConfiguration>();
	private List<ILaunchConfiguration> selectedConfigurationList = new LinkedList<ILaunchConfiguration>();
	private List<ILaunchConfiguration> typeConfigurationsList = new LinkedList<ILaunchConfiguration>();
    private Map<ILaunchConfiguration, Boolean> selectionMap = new HashMap<ILaunchConfiguration, Boolean>();
    private CheckboxTableViewer configurationCheckTable;    
    private TableViewer typeConfigurationsTable;
    private Button upButton;
    private Button downButton;
    private Button addButton;
    private Button removeButton;
    
	@Override
	public void createControl(Composite parent) {
		manager = LaunchConfigurationManager.getInstance();
				
		Font font = parent.getFont();
		Composite composite = new Composite(parent, SWT.NONE);
		int columns = 2;
		GridLayout layout = new GridLayout(columns, true);
		composite.setLayout(layout);
		composite.setFont(font);
		GridData layoutData = new GridData(GridData.FILL_BOTH | GridData.GRAB_HORIZONTAL | GridData.GRAB_HORIZONTAL);
		
		layoutData.minimumHeight = 200;
	    layoutData.minimumWidth = 250;
	    layoutData.heightHint = 200;
	    layoutData.widthHint = 250;
	    composite.setLayoutData(layoutData);
		
	    createConfigTypesListComponent(composite);
		createConfurationCheckTableComponent(composite);
		
		setControl(composite);
	}
	
	@Override
	public String getName() {
		return TAB_NAME;
	}
	
	@Override
	public void initializeFrom(ILaunchConfiguration config) {
		try {
			selectedConfigurationList.clear();
			configurationsList.clear();
			ConfigDataUtils.loadData(config, manager, selectionMap, configurationsList, selectedConfigurationList);
			configurationCheckTable.refresh();
			for (ILaunchConfiguration configuration : selectionMap.keySet()) {
				configurationCheckTable.setChecked(configuration, selectionMap.get(configuration));
			}
			configurationCheckTable.refresh();
			updateLaunchConfigurationDialog();
		} catch (Exception e) {
			e.printStackTrace();
			setErrorMessage(e.getMessage());
		}
	}

	@Override
	public void performApply(ILaunchConfigurationWorkingCopy config) {
		try {
			ConfigDataUtils.saveData(config, selectionMap, configurationsList);
		} catch (CoreException e) {
			e.printStackTrace();
			setErrorMessage(e.getMessage());
		}
	}
	
	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy arg0) {		
	}
	
	public Image getImage() {
		return DebugUITools.getImage(IInternalDebugUIConstants.IMG_OBJS_PERSPECTIVE_TAB);
	}
	
	private void createConfurationCheckTableComponent(Composite composite) {
		Group group = new Group(composite, SWT.NONE);
		group.setFont(composite.getFont());
		GridLayout layout = new GridLayout();
		group.setLayout(layout);
		group.setLayoutData(new GridData(GridData.FILL_BOTH));
		String controlName = "Current configuration";
		group.setText(controlName);
	
	    configurationCheckTable = CheckboxTableViewer.newCheckList(group, SWT.SINGLE | SWT.BORDER | SWT.RESIZE | SWT.V_SCROLL | SWT.H_SCROLL);
	
	    Table table = configurationCheckTable.getTable();
	    table.setHeaderVisible(true);
	    table.setLinesVisible(true);
	    table.setLayoutData(new GridData(GridData.FILL_BOTH));
	
	    TableColumn titleColumn = new TableColumn(table, SWT.NONE);
	    titleColumn.setText("Configuration Name");
	    titleColumn.setResizable(true);
	    titleColumn.setWidth(200);
	    configurationCheckTable.setContentProvider(new ArrayContentProvider());
	    configurationCheckTable.setInput(configurationsList);
	
	    configurationCheckTable.addCheckStateListener(new ICheckStateListener() {
	        public void checkStateChanged(CheckStateChangedEvent event) {
	            boolean checked = event.getChecked();
	            ILaunchConfiguration element = (ILaunchConfiguration) event.getElement();
	            selectionMap.put(element, Boolean.valueOf(checked));
	            updateLaunchConfigurationDialog();
	        }
	    });
	    
	    table.addSelectionListener(new SelectionAdapter() {
	        @Override
	        public void widgetSelected(SelectionEvent e) {
	        	handleCheckTableSelection();
	        	removeButton.setEnabled(true);
	        }
	    });
	
	    createCheckTableButtonList(group);
	}
	
	private void createCheckTableButtonList(Composite composite) {
		upButton = new Button(composite, SWT.PUSH | SWT.CENTER);
		initButton(composite, upButton, "Up", false, new SelectionAdapter() {
	        @Override
	        public void widgetSelected(SelectionEvent e) {
	            int index = configurationCheckTable.getTable().getSelectionIndex();
	            if (index != -1) {
	                moveUp(configurationsList.get(index));
	                configurationCheckTable.refresh();
	                handleCheckTableSelection();
	                updateLaunchConfigurationDialog();
	            }
	        }
	    });
		
		downButton = new Button(composite, SWT.PUSH | SWT.CENTER);
		initButton(composite, downButton, "Down", false, new SelectionAdapter() {
	        @Override
	        public void widgetSelected(SelectionEvent e) {
	            int index = configurationCheckTable.getTable().getSelectionIndex();
	            if (index != -1) {
	                moveDown(configurationsList.get(index));
	                configurationCheckTable.refresh();
	                handleCheckTableSelection();
	                updateLaunchConfigurationDialog();
	            }
	        }
	    });
		
		removeButton = new Button(composite, SWT.PUSH | SWT.CENTER);
		initButton(composite, removeButton, "Remove", false, new SelectionAdapter() {
	        @Override
	        public void widgetSelected(SelectionEvent e) {
	            int index = configurationCheckTable.getTable().getSelectionIndex();
	            if (index != -1) {
	            	ILaunchConfiguration configuration = (ILaunchConfiguration)((IStructuredSelection) configurationCheckTable.getSelection()).getFirstElement();
	            	configurationsList.remove(configuration);
	            	configurationCheckTable.refresh();
	            	updateLaunchConfigurationDialog();
	            }
	        }
	    });
	}
	
	private void moveUp(ILaunchConfiguration configuration) {
        int indexOf = configurationsList.indexOf(configuration);
        if (configurationsList.size() > 1 && indexOf >= 0) {
        	configurationsList.remove(indexOf);
        	configurationsList.add(indexOf - 1, configuration);
        }
    }
	
	private void moveDown(ILaunchConfiguration configuration) {
        int indexOf = configurationsList.indexOf(configuration);
        if (configurationsList.size() > 1) {
	        configurationsList.remove(indexOf);
	        configurationsList.add(indexOf + 1, configuration);
        }
    }
	
	private void createConfigTypesListComponent(Composite composite) {
		Group group = new Group(composite, SWT.NONE);
		group.setFont(composite.getFont());
		GridLayout layout = new GridLayout();
		group.setLayout(layout);
		group.setLayoutData(new GridData(GridData.FILL_BOTH));

		String controlName = "Launch Configurations";
		group.setText(controlName);

		Label label = new Label(group, SWT.NONE);
		label.setText("Configuration Type");

		GridData data = new GridData(200, GridData.FILL, true, false);
		label.setLayoutData(data);

		configurationTypesCombo = new Combo(group, SWT.READ_ONLY);

		data = new GridData();
		data.horizontalIndent = 0;
		
		typeNames = manager.getTypes().keySet();
		List<String> sortedTypeNames = new ArrayList<String>(typeNames.size());
		sortedTypeNames.addAll(typeNames);
		Collections.sort(sortedTypeNames);
		configurationTypesCombo.setItems(sortedTypeNames.toArray(new String[sortedTypeNames.size()]));
		configurationTypesCombo.setLayoutData(data);
		configurationTypesCombo.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int index = configurationTypesCombo.getSelectionIndex();
				for (String typeName : typeNames) {					
					String env = configurationTypesCombo.getItem(index);
					if (env.equals(typeName)) {
						ILaunchConfigurationType type = manager.getTypes().get(typeName);
						if (type != null) {
							fillConfigurationList(type);
						}
						break;
					}					
				}
				setDirty(true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});
		
		createTypeConfigurationsListComponent(group);
	}
	
	private void createTypeConfigurationsListComponent(Composite composite) {
		typeConfigurationsTable = new TableViewer(composite, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
	
	    Table table = typeConfigurationsTable.getTable();
	    table.setHeaderVisible(false);
	    table.setLinesVisible(false);
	    table.setLayoutData(new GridData(GridData.FILL_BOTH));
	    typeConfigurationsTable.setContentProvider(new ArrayContentProvider());
	    typeConfigurationsTable.setInput(typeConfigurationsList);
	    
	    table.addSelectionListener(new SelectionAdapter() {
	        @Override
	        public void widgetSelected(SelectionEvent e) {
	            handleTableSelection();
	        }
	    });
	    
	    createTableButtonList(composite);
	}
	
	private void createTableButtonList(Composite composite) {
		addButton = new Button(composite, SWT.PUSH | SWT.CENTER);
		initButton(composite, addButton, "Add", false, new SelectionAdapter() {
	        @Override
	        public void widgetSelected(SelectionEvent e) {
	            int index = typeConfigurationsTable.getTable().getSelectionIndex();
	            if (index != -1) {
	            	ILaunchConfiguration configuration = (ILaunchConfiguration)((IStructuredSelection) typeConfigurationsTable.getSelection()).getFirstElement();
	            	configurationsList.add(configuration);
	            	configurationCheckTable.refresh();
	            	removeButton.setEnabled(false);
	            	updateLaunchConfigurationDialog();
	            }
	        }
	    });
	}
	
	private void handleCheckTableSelection() {
		 ILaunchConfiguration configuration = (ILaunchConfiguration)((IStructuredSelection) configurationCheckTable.getSelection()).getFirstElement();
		 if (configuration != null) {
		     int index = configurationCheckTable.getTable().getSelectionIndex();
		     upButton.setEnabled(index > 0);
		     downButton.setEnabled(index < configurationsList.size() - 1);
		 } else {
		     upButton.setEnabled(false);
		     downButton.setEnabled(false);
		 }
	 }
	 
	 private void handleTableSelection() {
		 ILaunchConfiguration configuration = (ILaunchConfiguration)((IStructuredSelection) typeConfigurationsTable.getSelection()).getFirstElement();
		 if (configuration != null) {
			addButton.setEnabled(true);
		 }   
		 
	 }
		
	private void fillConfigurationList(ILaunchConfigurationType type) {
		ILaunchConfiguration[] configurations = manager.getConfigurations(type);
		typeConfigurationsList.clear();
		for (ILaunchConfiguration configuration : configurations) {
			typeConfigurationsList.add(configuration);
		}
		typeConfigurationsTable.refresh();
		addButton.setEnabled(false);
	}
	
	private void createInputComponent(Composite composite) {
		Label routeLabel = new Label(composite, SWT.NONE);
		routeLabel.setText("Text");
		route = new Text(composite, SWT.SINGLE | SWT.BORDER);
		GridData data = new GridData();
		data = new GridData();
		data.widthHint = 200;
		route.setLayoutData(data);
	}

	private void initButton(Composite composite, Button button, String text, boolean enabled, SelectionAdapter selectionAdapter) {
		button.setText(text);
		button.setEnabled(enabled);
		button.addSelectionListener(selectionAdapter);
	    GridData data = new GridData();
	    data.widthHint = 50;
	    data.horizontalAlignment = GridData.FILL;
	    button.setLayoutData(data);
	}
}

