package ru.ivanrigovsky.eclipse.compositelauncher.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;

import ru.ivanrigovsky.eclipse.compositelauncher.constants.ConfigConstant;
import ru.ivanrigovsky.eclipse.compositelauncher.launcher.LaunchConfigurationManager;

public class ConfigDataUtils {
	
	public static void saveData(ILaunchConfigurationWorkingCopy config, 
			Map<ILaunchConfiguration, Boolean> selectionMap, 
			List<ILaunchConfiguration> configurationsList) throws CoreException {
		List<String> configurations = new LinkedList<String>();
		List<String> configurationsSelected = new LinkedList<String>();
		
		for (ILaunchConfiguration configuration : configurationsList) {
			configurations.add(configuration.getName());
			
			//selected list
			if (selectionMap.get(configuration) != null && selectionMap.get(configuration)) {
				configurationsSelected.add(configuration.getName());
			}
		}
		
		config.setAttribute(ConfigConstant.CONFIGURATIONS_LIST, configurations);		
		config.setAttribute(ConfigConstant.CONFIGURATIONS_LIST_SELECTED, configurationsSelected);
		
	}
	
	public static void loadData(ILaunchConfiguration config, LaunchConfigurationManager manager,
								Map<ILaunchConfiguration, Boolean> selectionMap, 
								List<ILaunchConfiguration> configurationsList,
								List<ILaunchConfiguration> selectedConfigurationList) throws CoreException {
		
		List<String> configurations = config.getAttribute(ConfigConstant.CONFIGURATIONS_LIST, new LinkedList<String>());
		List<String> configurationsSelected = config.getAttribute(ConfigConstant.CONFIGURATIONS_LIST_SELECTED, new LinkedList<String>());
		for (String configurationName : configurations) {
			ILaunchConfiguration configuration = manager.getConfiguration(configurationName);
			if (configurationsList != null) {
				configurationsList.add(configuration);
			}
			
			if (configurationsSelected.contains(configurationName)) {
				if (selectedConfigurationList != null)
					selectedConfigurationList.add(configuration);
				if (selectionMap != null)
					selectionMap.put(configuration, true);
			} else {
				if (selectionMap != null)
					selectionMap.put(configuration, false);
			}
		}
	}		

}
